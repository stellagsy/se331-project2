import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewActTeacherComponent } from './view-act-teacher.component';

describe('ViewActTeacherComponent', () => {
  let component: ViewActTeacherComponent;
  let fixture: ComponentFixture<ViewActTeacherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewActTeacherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewActTeacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
