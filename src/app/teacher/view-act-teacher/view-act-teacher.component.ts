import { Component, OnInit, ViewChild } from '@angular/core';
import { EnrollTableDataSource } from './activity-table-datasource';
import { BehaviorSubject } from 'rxjs';
import {  MatSort, MatPaginator } from '@angular/material';
import { Enroll } from 'src/app/_models/enroll';
import { UserService, AlertService } from 'src/app/_services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-view-act-teacher',
  templateUrl: './view-act-teacher.component.html',
  styleUrls: ['./view-act-teacher.component.css']
})
export class ViewActTeacherComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource : EnrollTableDataSource;
  filter: string;
  filter$: BehaviorSubject<string>;
  enrolls: Enroll[] = [];
  enrollForm:FormGroup;
  showForm = false;
  studentId;
  actName;
  actTeacher;
  status;
 

  displayedColumns: string[] = [ 'actName', 'actTeacher','studentId', 'actStatus', 'accept'];
  constructor(private userService: UserService, 
    private formBuilder:FormBuilder,
    private alertService:AlertService) { }

  ngOnInit() {
    this.userService.getEnrollments()
    .subscribe(enrolls => {
      this.dataSource = new EnrollTableDataSource(this.paginator, this.sort);
      this.dataSource.data = enrolls;
      this.enrolls = enrolls;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
    }
    );
    this.enrollForm = this.formBuilder.group({
      actName:['', Validators.required],
      actTeacher:[''],
      studentId:[''],
      status:['', Validators.required],
     
  });
  }
  isDisabled(e){
      if (e.status=="accepted")
      return true;
      else{
        return false;
      }
  }
  private loadEnrolls() {
    this.userService.getEnrollments().pipe(first()).subscribe(enrolls => { 
        this.enrolls = enrolls; 
        
    });
  }
  private deleteEnroll(id: number) {
    this.userService.deleteEnroll(id).pipe(first()).subscribe(() => { 
          window.location.reload();
          });
  }
 
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }
  currentEnrollId:number
  get f() { return this.enrollForm.controls; }
  accept(e){
    this.studentId = e.studentId;
    this.actName = e.actName;
    this.actTeacher = e.actTeacher;
    this.showForm = true;
    this.currentEnrollId = e.id
    this.loadEnrolls();

  }
  onAccept(){
    this.deleteEnroll(this.currentEnrollId)
    this.userService.enroll(this.enrollForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.showForm = false;
                },
                error => {
                  alert("error")
                    this.alertService.error(error);
                });
    }
  }
