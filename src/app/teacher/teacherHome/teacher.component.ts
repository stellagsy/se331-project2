import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models';
import { UserService } from 'src/app/_services';
import { Teacher } from 'src/app/_models/teacher';
import { TeacherService } from 'src/app/_services/teacher.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {
  currentTeacher: Teacher;
  
  teachers:Teacher[] =[]
  constructor(private teacherService: TeacherService) {
    this.currentTeacher = JSON.parse(localStorage.getItem('currentTeacher'));
}
getId(){
  
}

ngOnInit() {
  this.loadAllTeachers();
}
private loadAllTeachers() {
  this.teacherService.getAll().pipe(first()).subscribe(teachers => { 
      this.teachers = teachers; 
  });
}

}
