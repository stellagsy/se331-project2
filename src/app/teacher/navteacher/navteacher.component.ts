import { Component, OnInit } from '@angular/core';
import { Teacher } from 'src/app/_models/teacher';
import { UserService } from 'src/app/_services';
import { TeacherService } from 'src/app/_services/teacher.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-navteacher',
  templateUrl: './navteacher.component.html',
  styleUrls: ['./navteacher.component.css']
})
export class NavteacherComponent implements OnInit {

  currentTeacher: Teacher;
  teachers: Teacher[] = [];
   
  constructor(private teacherService: TeacherService) {
    this.currentTeacher = JSON.parse(localStorage.getItem('currentTeacher'));
    
}

  ngOnInit() {
    this.loadAllTeachers();
    
    
  }
  private loadAllTeachers() {
    this.teacherService.getAll().pipe(first()).subscribe(teachers => { 
        this.teachers = teachers; 
    });
  }
}
