﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '../_models';
import { UserService, AlertService, ActivityService } from '../_services';
import { RegisterComponent } from '../register';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { Activity } from '../_models/activity';

@Component({templateUrl: 'home.component.html',
styleUrls: ['./home.component.css']})

export class HomeComponent implements OnInit {
    submitted = false;
    loading = false;
    regActForm: FormGroup;
    regComp: RegisterComponent;
    currentUser: User;
    users: User[] = [];
    acts: Activity[] = [];

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private userService: UserService,
        private actservice: ActivityService,
        private alertService: AlertService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    }

    ngOnInit() {
        this.loadAllUsers();
        this.loadAllActs();
        this.regActForm = this.formBuilder.group({
            name: ['', Validators.required],
            location: ['', Validators.required],
            description: ['', Validators.required],
            sdate: ['', Validators.required],
            edate: ['', Validators.required],
            date: ['', Validators.required],
            time: ['', Validators.required],
            teacher: ['', Validators.required]
        });

    }


    deleteUser(id: number) {
        this.userService.delete(id).pipe(first()).subscribe(() => {
            this.loadAllUsers();
        });
    }
    deleteAct(id: number) {
        this.actservice.deleteAct(id).pipe(first()).subscribe(() => {
            this.loadAllActs();
        });
    }

    private loadAllUsers() {
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.users = users;

        });
    }
    private loadAllActs() {
        this.actservice.getAll().pipe(first()).subscribe(acts => {
            this.acts = acts;

        });
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.regActForm.invalid) {
            return;
        }
        console.log(this.regActForm);
        alert('registering');
        this.loading = true;
        this.actservice.registerAct(this.regActForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                }, or => {
                    this.alertService.error('error');
                    this.loading = false;
                });
    }


}
