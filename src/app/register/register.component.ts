﻿import { Component, OnInit, wtfStartTimeRange } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, UserService } from '../_services';
import {RegisterModel} from '../_models/register.model'
@Component({templateUrl: 'register.component.html',
styleUrls: ['./register.component.css']})

export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    private _hide = true;
    

 

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService) { }
    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            name: ['', Validators.required],
            surname: ['', Validators.required],
            studentId: ['', Validators.required],
            image: ['', Validators.required],
            dateOfBirth: ['', Validators.required],
            username: ['', Validators.required],
            password: ['', Validators.required],
            cpassword: ['', Validators.required],
        },{
            validator: RegisterComponent.MatchPassword // your validation method
        });
    }
    static MatchPassword(AC: AbstractControl) {
        let password = AC.get('password').value; // to get value in input tag
        let confirmPassword = AC.get('cpassword').value; // to get value in input tag
         if(password != confirmPassword) {
             console.log('false');
             AC.get('cpassword').setErrors( {MatchPassword: true} )
         } else {
             console.log('true');
             return null
         }
     }


    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }
    
    
    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }


        this.loading = true;
        this.userService.register(this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

}
                
