import { Time } from "@angular/common";
import { Options } from "selenium-webdriver/safari";
import { User } from ".";
import { MatButton } from "@angular/material";
import { Teacher } from "./teacher";

export class Activity {
    id: number;
    name: string;
    location: string;
    description: string;
    sdate: Date;
    edate: Date;
    date: Date;
    time: Time;
    teacher: Teacher;  
    enrolledStu = new Array<User>();
    
    
    
}