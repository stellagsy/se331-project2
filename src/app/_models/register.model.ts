import { Url } from "url";

export class RegisterModel {
    name: string;
    surname: number;
    studentId: string;
    image: Url;
    dateOfBirth: Date;
    email: string; 
    password: string;
    cpassword: string;
}
