import { Activity } from "./activity";

export class Teacher {
    id: number;
    username: string;
    password:string;
    assignedActs = new Array<Activity>();
}