﻿import { Activity } from "./activity";
import { identifierModuleUrl } from "@angular/compiler";

export class User {
    name: string;
    id: number;
    username: string;
    password: string;
    firstName: string;
    dateOfBirth:Date;
    lastName: string; 
    role: string;
    image: string;
    

}