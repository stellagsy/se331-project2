﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule, NG_VALUE_ACCESSOR }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AlertComponent } from './_directives';
import { AuthGuard } from './_guards';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AlertService, AuthenticationService, UserService, ActivityService } from './_services';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { StudentComponent } from './student/student.component';
import { TeacherComponent } from './teacher/teacherHome/teacher.component';;
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    MatToolbarModule, MatButtonModule, MatSidenavModule
    , MatIconModule, MatListModule, MatGridListModule, MatCardModule
    , MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, 
    MatFormFieldModule, MatProgressSpinnerModule, MatOptionModule, 
    MatSelectModule, MatDatepicker, MatDatepickerModule, MatNativeDateModule, MatChip, MatChipsModule, MatTabsModule, MatTabGroup,
  } from '@angular/material';
  import {MatInputModule} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { MyNavComponent } from './my-nav/my-nav.component';;
import { ActivityComponent } from './activity/activity.component';
import { AdminNavBarComponent } from './admin/admin-nav-bar/admin-nav-bar.component';
import { RegisterTeacherComponent } from './admin/register-teacher/register-teacher.component';
import { RegisterActivityComponent } from './admin/register-activity/register-activity.component';
import { HomeAdminComponent } from './admin/home/homeAdmin.component';
import { ViewActivityComponent } from './admin/adminView-activity/adminView-activity.component';
import { ActivityDetailComponent } from './admin/activity-detail/activity-detail.component';
import { TeacherService } from './_services/teacher.service';;
import { StudentNavBarComponent } from './student/student-nav-bar/student-nav-bar/student-nav-bar.component';
import {  ListAct } from './list/list.component';
import { EnrollComponent } from './student/enroll/enroll.component';
import { StudentViewActivityComponent } from './student/student-view-activity/student-view-activity.component';
import { NavteacherComponent } from './teacher/navteacher/navteacher.component';
import { ViewActTeacherComponent } from './teacher/view-act-teacher/view-act-teacher.component';;
import { AboutActivityComponent } from './student/about-activity/about-activity.component'
@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        routing,
        FormsModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatGridListModule,
        MatCardModule,
        MatMenuModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatInputModule,
        MatFormFieldModule,
        LayoutModule,
        RouterModule,
        MatProgressSpinnerModule,
        MatOptionModule,
        MatSelectModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        RouterModule,MatChipsModule,
        MatTabsModule
        
       ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeAdminComponent,
        LoginComponent,
        RegisterComponent,
        StudentComponent ,
        TeacherComponent,
        MyNavComponent,
        ActivityComponent,
        AdminNavBarComponent,
        RegisterTeacherComponent,
        RegisterActivityComponent,
        ActivityDetailComponent,
        RegisterActivityComponent,
        StudentNavBarComponent,
        ListAct,
        ViewActivityComponent,
        EnrollComponent,
        StudentViewActivityComponent,
        ViewActTeacherComponent,
        NavteacherComponent,
        AboutActivityComponent,
        
],
        
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        ActivityService,
        TeacherService,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }