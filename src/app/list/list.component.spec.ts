import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAct } from './list.component';

describe('ListComponent', () => {
  let component: ListAct;
  let fixture: ComponentFixture<ListAct>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAct]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAct);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
