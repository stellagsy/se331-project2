import { Component, OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { Activity } from 'src/app/_models/activity';
import { ActivityService, UserService, AlertService } from 'src/app/_services';
import {  MatSort, MatPaginator } from '@angular/material';
import { BehaviorSubject, Observable } from 'rxjs';
import { ActivityTableDataSource } from '../list/list-table-datasource'
import { Button } from '../_models/button';
import { User } from '../_models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Enroll } from '../_models/enroll';




@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListAct implements OnInit {
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  currentUser: User;
  studentId = 0
  actName = '';
  actTeacher = '';
  status = 'pending';
  currentStudentId = ''
  users: User[] = [];
  acts: Activity[] = [];
  enrolls: Enroll[] = [];
  disableForm = false;
  displayedColumns: string[] = [ 'name', 'location', 'description','sdate','edate','date','time','teacher','enroll'];
  dataSource : ActivityTableDataSource;
  filter: string;
  filter$: BehaviorSubject<string>;
  loading = false;
  enrollForm:FormGroup;
  constructor(private actService: ActivityService, 
              private userService: UserService,
              private formBuilder: FormBuilder,
              private alertService: AlertService,
              private router: Router) { 
              this.loadAllUsers();
              this.loadEnrollments();
              this.currentUser = JSON.parse(localStorage.getItem('currentUser'));}
  
ngOnInit() {
  this.actService.getAll()
    .subscribe(acts => {
      this.dataSource = new ActivityTableDataSource(this.paginator, this.sort);
      this.dataSource.data = acts;
      this.acts = acts;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
    });
    this.enrollForm = this.formBuilder.group({
      studentId:[''],
      actName:['', Validators.required],
      actTeacher:[''],
      status:['', Validators.required],
     
  });
  
}
deleteEnrollments(){
  this.userService.deleteEnrollments();
  window.location.reload();
}
private loadAllUsers() {
  this.userService.getAll().pipe(first()).subscribe(users => { 
      this.users = users; 
  });
}


private loadEnrollments() {
  this.userService.getEnrollments().pipe(first()).subscribe(enrolls => { 
      this.enrolls = enrolls; 
      
  });
}
applyFilter(filterValue: string) {
  this.filter$.next(filterValue.trim().toLowerCase());
}
getValues(actName,actTeacher){
  this.actName = actName;
  this.actTeacher = actTeacher;
  this.disableForm = true;
}

get f() { return this.enrollForm.controls; }
onEnroll(){{
    this.loading = true;
        this.userService.enroll(this.enrollForm.value)
            .pipe(first())
            .subscribe(
                data => {
                  alert("registered")
                    this.alertService.success('Registration successful', true);
                    this.loadEnrollments()
                    this.disableForm = false;
                    this.loading= false;
                },
                error => {
                  alert("error")
                    this.alertService.error(error);
                    this.loading = false;
                    this.disableForm = false;
                    this.loading= false;
                });
    }
  
}}


