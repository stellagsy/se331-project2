﻿import { Component, OnInit, Pipe } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { AlertService, AuthenticationService, UserService } from '../_services';
import { User } from '../_models';
import { ErrorStateMatcher } from '@angular/material/core';
import { LoginModel } from '../_models/login.model';
import { Teacher } from '../_models/teacher';
import { TeacherService } from '../_services/teacher.service';

@Component({templateUrl: 'login.component.html',
            styleUrls: ['./login.component.css']
        })
export class LoginComponent implements OnInit {
    user: LoginModel = new LoginModel();
    teachers: Teacher[] = [];
    users : User[] = [];
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    selectedRole = 'option1'; //can be changed with 2 way binding
    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private teacherService: TeacherService,
        private userService: UserService
        ) {}
        

       
        
    ngOnInit() {
        // this.loadAllTeachers();
        // this.loadAllUsers();
        this.loginForm = this.formBuilder.group({
            'username': ['', Validators.required],
            'password': ['', Validators.required],
            'role': ['', Validators.required],
        });

        // reset login status
        this.authenticationService.logout();
        this.getUsers();


    }
    private loadAllTeachers() {
        this.teacherService.getAll().pipe(first()).subscribe(teachers => { 
            this.teachers = teachers; 
            
        });
    }
    private loadAllUsers() {
        this.userService.getAll().pipe(first()).subscribe(users => { 
            this.users = users; 
            
        });
    }


    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }
    getUsers(): Observable<User[]> {
        return of(this.users)
      }
      onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.loading = true;
        if(this.f.role.value==="Student"){
            this.authenticationService.loginUser(this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    if (this.f.username.value==="admin@gmail.com" && this.f.password.value==="adminadmin"){
                        this.router.navigate(['admin']);
                    }else{
                    this.router.navigate(['student/' , this.f.username.value]);
                    }
                
                },
                error => {
                    this.alertService.error(error);
                    alert("Incorrect password")
                    this.loading = false;
                });
    }
    else if(this.f.role.value==="Teacher"){
        this.authenticationService.loginTeacher(this.f.username.value, this.f.password.value)
        .pipe(first())
        .subscribe(
            data => {
                if (this.f.username.value==="admin@gmail.com" && this.f.password.value==="adminadmin"){
                    this.router.navigate(['admin']);
                }else{
                this.router.navigate(['teacher/' , this.f.username.value]);
                }
            
            },
            error => {
                this.alertService.error(error);
                alert("Check information again please!")
                this.loading = false;
            });
}
}}
    

