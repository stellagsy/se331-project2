﻿import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { ActivityService } from '../_services';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let countId = 0;
        // array in local storage for registered users
        let users: any[] = JSON.parse(localStorage.getItem('users')) || [];

        // array in local storage for registered activities
        let acts: any[] = JSON.parse(localStorage.getItem('acts')) || [];

        // array in local storage for registered teachers
        let teachers: any[] = JSON.parse(localStorage.getItem('teachers')) || [];

         // array in local storage for registered teachers
         let enrolls: any[] = JSON.parse(localStorage.getItem('enrolls')) || [];




        
        // wrap in delayed observable to simulate server api call
        return of(null).pipe(mergeMap(() => {

            // authenticate user
            if (request.url.endsWith('/users/authenticate') && request.method === 'POST') {
                // find if any user matches login credentials
                let filteredUsers = users.filter(user => {
                    return user.email === request.body.email && user.password === request.body.password;
                });

                if (filteredUsers.length) {
                    // if login details are valid return 200 OK with user details and fake jwt token
                    let user = filteredUsers[0];
                    let body = {
                        id: user.id,
                        email: user.email,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        role: user.role,
                        token: 'fake-jwt-token'
                    };

                    return of(new HttpResponse({ status: 200, body: body }));
                } else {
                    // else return 400 bad request
                    return throwError({ error: { message: 'Username or password is incorrect' } });
                }
            }
            // authenticate teacher
            if (request.url.endsWith('/teachers/authenticate') && request.method === 'POST') {
                // find if any teacher matches login credentials
                let filteredTeachers = teachers.filter(teacher => {
                    return teacher.email === request.body.email && teacher.password === request.body.password;
                });

                if (filteredTeachers.length) {
                    // if login details are valid return 200 OK with user details and fake jwt token
                    let teacher = filteredTeachers[0];
                    let body = {
                        id: teacher.id,
                        email: teacher.email,
                        firstName: teacher.firstName,
                        lastName: teacher.lastName,
                        role: teacher.role,
                        token: 'fake-jwt-token'
                    };

                    return of(new HttpResponse({ status: 200, body: body }));
                } else {
                    // else return 400 bad request
                    return throwError({ error: { message: 'Username or password is incorrect' } });
                }
            }

            // get users
            if (request.url.endsWith('/users') && request.method === 'GET') {
                // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    return of(new HttpResponse({ status: 200, body: users }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }

            // get teachers
            if (request.url.endsWith('/teachers') && request.method === 'GET') {
                // check for fake auth token in header and return teachers if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    return of(new HttpResponse({ status: 200, body: teachers }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }

            // get acts
            if (request.url.endsWith('/acts') && request.method === 'GET') {
                // check for fake auth token in header and return acts if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    return of(new HttpResponse({ status: 200, body: acts }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }
            // get enrollments
            if (request.url.endsWith('/enrolls') && request.method === 'GET') {
                // check for fake auth token in header and return enroll if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    return of(new HttpResponse({ status: 200, body: enrolls }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }
            // update(user: User) {
            //     return this.http.put(`${environment.apiUrl}/users/` + user.id, user);
            // }

            // get enrolls by id
            if (request.url.match(/\/enrolls\/\d+$/) && request.method === 'GET') {
                // check for fake auth token in header and return enroll if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find enroll by id in enrolls array
                    let urlParts = request.url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);
                    let matchedEnrolls = enrolls.filter(enroll => { return enroll.id === id; });
                    let enroll = matchedEnrolls.length ? matchedEnrolls[0] : null;

                    return of(new HttpResponse({ status: 200, body: enroll }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }
                // update enrollment
            if (request.url.match(/\/enrolls\/\d+$/) && request.method === 'PUT') {
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find user by id in users array
                    let urlParts = request.url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);
                    let matchedUsers = users.filter(user => { return user.id === id; });
                    let user = matchedUsers.length ? matchedUsers[0] : null;
                    // save new user
                    let newEnroll = request.body;
                    // newUser.role = request.body.role.value;
                    newEnroll.status = "gggggggggggggggggg"
                    alert( newEnroll.status)
                    // enrolls.push(newEnroll);
                    // localStorage.setItem('enrolls', JSON.stringify(users));
                    return of(new HttpResponse({ status: 200, body: user }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }

            // get user by id
            if (request.url.match(/\/users\/\d+$/) && request.method === 'GET') {
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find user by id in users array
                    let urlParts = request.url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);
                    let matchedUsers = users.filter(user => { return user.id === id; });
                    let user = matchedUsers.length ? matchedUsers[0] : null;

                    return of(new HttpResponse({ status: 200, body: user }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }
            // get teacher by id
            if (request.url.match(/\/teachers\/\d+$/) && request.method === 'GET') {
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find user by id in users array
                    let urlParts = request.url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);
                    let matchedTeachers = teachers.filter(teacher => { return teacher.id === id; });
                    let user = matchedTeachers.length ? matchedTeachers[0] : null;

                    return of(new HttpResponse({ status: 200, body: user }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }


             // get act by id
             if (request.url.match(/\/acts\/\d+$/) && request.method === 'GET') {
                // check for fake auth token in header and return act if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find act by id in users array
                    let urlParts = request.url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);
                    let matchedActs = acts.filter(act => { return act.id === id; });
                    let act = matchedActs.length ? matchedActs[0] : null;

                    return of(new HttpResponse({ status: 200, body: act }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }

            // register user
            if (request.url.endsWith('/users/register') && request.method === 'POST') {
                // get new user object from post body
                let newUser = request.body;

                // validation
                let duplicateUser = users.filter(user => { return user.username === newUser.username; }).length;
                if (duplicateUser) {
                    return throwError({ error: { message: 'Username "' + newUser.username + '" is already taken' } });
                }

                // save new user
                newUser.id = users.length + 1;
                // newUser.role = request.body.role.value;
                users.push(newUser);
                localStorage.setItem('users', JSON.stringify(users));

                // respond 200 OK
                return of(new HttpResponse({ status: 200 }));
            }

             // register teacher
             if (request.url.endsWith('/teachers/register') && request.method === 'POST') {
                // get new teacher object from post body
                let newTeacher = request.body;

                // validation
                let duplicateTeacher = teachers.filter(teacher => { return teacher.username === newTeacher.username; }).length;
                if (duplicateTeacher) {
                    return throwError({ error: { message: 'Name "' + newTeacher.username + '" is already taken' } });
                }

                // save new teacher
                newTeacher.id = teachers.length + 1;
                // newTeacher.role = request.body.role.value;
                teachers.push(newTeacher);
                localStorage.setItem('teachers', JSON.stringify(teachers));

                // respond 200 OK
                return of(new HttpResponse({ status: 200 }));
            }
            
            
            
            // register Activity
            if (request.url.endsWith('/acts/register') && request.method === 'POST') {
                // get new activity object from post body
                let newAct = request.body;

                // validation
                let duplicateAct = acts.filter(act => { return act.id === newAct.id; }).length;
                if (duplicateAct) {
                    return throwError({ error: { message: 'Activity "' + newAct.id + '" is already taken' } });
                }
                
                newAct.id = acts.length+1; 
                // newUser.role = request.body.role.value;
                acts.push(newAct);
                localStorage.setItem('acts', JSON.stringify(acts));
                // respond 200 OK
                return of(new HttpResponse({ status: 200 }));
                
            }
             // enroll 
             if (request.url.endsWith('/enrolls/enroll') && request.method === 'POST') {
                // get new activity object from post body
                let newEnroll = request.body;
                // validation
                let duplicateEnroll = enrolls.filter(enroll => { return enroll.id === newEnroll.id; }).length;
                if (duplicateEnroll) {
                    return throwError({ error: { message: 'Enrollment "' + newEnroll.id + '" is have already have' } });
                }
                
                newEnroll.id = enrolls.length+1; 
                // newUser.role = request.body.role.value;
                enrolls.push(newEnroll);
                localStorage.setItem('enrolls', JSON.stringify(enrolls));
                // respond 200 OK
                return of(new HttpResponse({ status: 200 }));
                
            }
           

            // delete user
            if (request.url.match(/\/users\/\d+$/) && request.method === 'DELETE') {
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find user by id in users array
                    let urlParts = request.url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);
                    for (let i = 0; i < users.length; i++) {
                        let user = users[i];
                        if (user.id === id) {
                            // delete user
                            users.splice(i, 1);
                            localStorage.setItem('users', JSON.stringify(users));
                            break;
                        }
                    }

                    // respond 200 OK
                    return of(new HttpResponse({ status: 200 }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }// delete teacher
            if (request.url.match(/\/teachers\/\d+$/) && request.method === 'DELETE') {
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find teacher by id in users array
                    let urlParts = request.url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);
                    for (let i = 0; i < teachers.length; i++) {
                        let teacher = teachers[i];
                        if (teacher.id === id) {
                            // delete teacher
                            teachers.splice(i, 1);
                            localStorage.setItem('teachers', JSON.stringify(teachers));
                            break;
                        }
                    }

                    // respond 200 OK
                    return of(new HttpResponse({ status: 200 }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }
            // delete enroll
            if (request.url.match(/\/enrolls\/\d+$/) && request.method === 'DELETE') {
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find enroll by id in enrolls array
                    let urlParts = request.url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);
                    for (let i = 0; i < enrolls.length; i++) {
                        let enroll = enrolls[i];
                        if (enroll.id === id) {
                            // delete enroll
                            enrolls.splice(i, 1);
                            localStorage.setItem('enrolls', JSON.stringify(enrolls));
                            break;
                        }
                    }

                    // respond 200 OK
                    return of(new HttpResponse({ status: 200 }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }

            // delete activity  
            if (request.url.match(/\/acts\/\d+$/) && request.method === 'DELETE') {
                // check for fake auth token in header and return act if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find act by id in acts array
                    let urlParts = request.url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);
                    for (let i = 0; i < acts.length; i++) {
                        let act = acts[i];
                        if (act.id === id) {
                            // delete act
                            acts.splice(i, 1);
                            localStorage.setItem('acts', JSON.stringify(acts));
                            break;
                        }
                    }
            

                    // respond 200 OK
                    return of(new HttpResponse({ status: 200 }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }

            // pass through any requests not handled above
            return next.handle(request);
            
        }))

        // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
        .pipe(materialize())
        .pipe(delay(500))
        .pipe(dematerialize());
    }
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};