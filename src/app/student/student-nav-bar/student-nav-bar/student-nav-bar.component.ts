import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models';
import { UserService } from 'src/app/_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-student-nav-bar',
  templateUrl: './student-nav-bar.component.html',
  styleUrls: ['./student-nav-bar.component.css']
})
export class StudentNavBarComponent implements OnInit {

  currentStudent: User;
  students:User[] =[]
  constructor(private userService: UserService) {
    this.currentStudent = JSON.parse(localStorage.getItem('currentUser'));
}
ngOnInit() {
  this.loadAllStudents();
}
private loadAllStudents() {
  this.userService.getAll().pipe(first()).subscribe(students => { 
      this.students = students; 
  });
}
}
