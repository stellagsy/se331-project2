import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentViewActivityComponent } from './student-view-activity.component';

describe('StudentViewActivityComponent', () => {
  let component: StudentViewActivityComponent;
  let fixture: ComponentFixture<StudentViewActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentViewActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentViewActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
