import { Component, OnInit } from '@angular/core';
import { Activity } from '../_models/activity';
import { User } from '../_models';

import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivityService, AlertService } from '../_services';

import { UserService } from '../_services';
import { first } from 'rxjs/operators';
import { HomeAdminComponent } from '../admin/home/homeAdmin.component';
import { Router } from '@angular/router';
 

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  currentUser: User;
  users: User[] = [];
  showForm = false;
  updateForm:FormGroup
  loading = false;
  constructor(private userService: UserService, private formBuilder:FormBuilder,
    private alertService:AlertService,private router:Router) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    
}

  ngOnInit() {
    this.loadAllUsers();
    this.updateForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      studentId: ['', Validators.required],
      image: ['', Validators.required],
      dateOfBirth: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      cpassword: ['', Validators.required],
  },{
      validator: StudentComponent.MatchPassword // your validation method
  });
}
static MatchPassword(AC: AbstractControl) {
  let password = AC.get('password').value; // to get value in input tag
  let confirmPassword = AC.get('cpassword').value; // to get value in input tag
   if(password != confirmPassword) {
       console.log('false');
       AC.get('cpassword').setErrors( {MatchPassword: true} )
   } else {
       console.log('true');
       return null
   }
}
    
  private loadAllUsers() {
    this.userService.getAll().pipe(first()).subscribe(users => { 
        this.users = users; 
    });
  }
  get f() { return this.updateForm.controls; }
  update(){
    this.showForm = true;
  }
    
    onUpdate() {

        // stop here if form is invalid
        if (this.updateForm.invalid) {
            return;
        }


        this.loading = true;
        this.userService.register(this.updateForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

}
