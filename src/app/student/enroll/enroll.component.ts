import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models';
import { first } from 'rxjs/operators';
import { UserService } from 'src/app/_services';
import { Enroll } from 'src/app/_models/enroll';



@Component({
  selector: 'app-enroll',
  templateUrl: './enroll.component.html',
  styleUrls: ['./enroll.component.css']
})
export class EnrollComponent implements OnInit {
  actName="vse";
  actTeacher="vs";
  actStatus="fhus";
  currentStudent: User;
  students:User[] =[];
  enrolls: Enroll[] = [];
  
  
  constructor(private userService: UserService) {
    this.currentStudent = JSON.parse(localStorage.getItem('currentUser'));
}
ngOnInit() {
  this.loadAllTeachers();
  this.loadEnrollments();
}
private loadAllTeachers() {
  this.userService.getAll().pipe(first()).subscribe(students => { 
      this.students = students; 
  });
}
private loadEnrollments(){
  this.userService.getEnrollments().pipe(first()).subscribe(enrolls => { 
    this.enrolls = enrolls; 
});
}

}
