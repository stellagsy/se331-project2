import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-activity',
  templateUrl: './about-activity.component.html',
  styleUrls: ['./about-activity.component.css']
})
export class AboutActivityComponent implements OnInit {
  showMyIdea = false;
  showYourIdea = true;
  myWords="";
  constructor() { }


  ngOnInit() {
  }
  onAdd(myWords){
    this.showMyIdea = true;
    this.showYourIdea = false;
    this.myWords = myWords

  }

}
