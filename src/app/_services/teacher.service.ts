import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { Teacher } from "../_models/teacher";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class TeacherService {
    constructor(private http: HttpClient) { }


    getAll() {
        return this.http.get<Teacher[]>(`${environment.apiUrl}/teachers`);
    }

    registerTeacher(teacher: Teacher) {
        return this.http.post(`${environment.apiUrl}/teachers/register`, teacher);
    }
    deleteTeacher(id: number) {
        return this.http.delete(`${environment.apiUrl}/teachers/` + id);
    }
    getTeacherById(id: number) {
        return this.http.get(`${environment.apiUrl}/teachers/` + id);
    }
    
    
}