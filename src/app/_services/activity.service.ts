import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Activity } from '../_models/activity';
import { Observable } from 'rxjs';

@Injectable()
export class ActivityService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Activity[]>(`${environment.apiUrl}/acts`);
    }

    registerAct(act: Activity) {
        return this.http.post(`${environment.apiUrl}/acts/register`, act);
    }
    deleteAct(id: number) {
        return this.http.delete(`${environment.apiUrl}/acts/` + id);
    }
    getActById(id: number) {
        return this.http.get(`${environment.apiUrl}/acts/` + id);
    }
    
    deleteAllActs(){
        localStorage.removeItem('acts')
    }
    updateAct(act: Activity): Observable<any> {
        return this.http.put(`${environment.apiUrl}/acts/` + act.id, act);
    }
}