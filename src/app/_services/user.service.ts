﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User } from '../_models';
import { Observable } from 'rxjs/internal/Observable';
import { Enroll } from '../_models/enroll';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }


    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/users`);
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/users/` + id);
    }
    
    register(user: User) {
        return this.http.post(`${environment.apiUrl}/users/register`, user);
    }

    update(user: User) {
        return this.http.put(`${environment.apiUrl}/users/` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/users/` + id);
    }
    enroll(enroll: Enroll) {
        return this.http.post(`${environment.apiUrl}/enrolls/enroll`, enroll);
    }
    getEnrollments() {
        return this.http.get<Enroll[]>(`${environment.apiUrl}/enrolls`);
    }
    deleteEnrollments(){
        localStorage.removeItem('enrolls') 
    }
    getEnrollById(id:number){
        return this.http.get(`${environment.apiUrl}/enrolls/` + id);
    }
    updateEnroll(enroll: Enroll) {
        return this.http.put(`${environment.apiUrl}/enrolls/` + enroll.actName, enroll);
    }
    deleteEnroll(id: number) {
        return this.http.delete(`${environment.apiUrl}/enrolls/` + id);
    }


}