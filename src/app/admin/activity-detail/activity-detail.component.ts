import { Component, OnInit, Input } from '@angular/core';
import { ActivityService } from 'src/app/_services';
import { Activity } from 'src/app/_models/activity';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-activity-detail',
  templateUrl: './activity-detail.component.html',
  styleUrls: ['./activity-detail.component.css']
})
export class ActivityDetailComponent implements OnInit {
  // acts: Activity[] = [];
  @Input() act: Activity
  constructor(private actService: ActivityService,private route: ActivatedRoute,
    private location: Location
    ) { }

  ngOnInit() {
  }
    


  goBack(): void {
    this.location.back();
  }
  


}
