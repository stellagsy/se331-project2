import { Component, OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { Activity } from 'src/app/_models/activity';
import { ActivityService, AlertService } from 'src/app/_services';
import {  MatSort, MatPaginator } from '@angular/material';
import { ActivityTableDataSource } from './activity-table-datasource';
import { BehaviorSubject } from 'rxjs';
import { FormGroup, FormBuilder, NgForm } from '@angular/forms';



@Component({
  selector: 'app-view-activity',
  templateUrl: './adminView-activity.component.html',
  styleUrls: ['./adminView-activity.component.css']
})
export class ViewActivityComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  acts: Activity[] = [];
  displayedColumns: string[] = [ 'id','name', 'location', 'description','sdate','edate','date',
  'time','teacher','enrolledStudent','edit','delete'];
  dataSource : ActivityTableDataSource;
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private actService: ActivityService, private formBuilder:FormBuilder,private alertService:AlertService) { }
  currentActId;currentActName;currentActLocation;currentActDescription;currentActSDate;
  currentActEDate;currentActDate;currentActTime;currentActTeacher;
  showEditForm = false;
  submitted = false;
  loading = true;
  actUpdateForm:FormGroup;
  newForm:FormGroup;
  currentAct = this.actService.getActById(this.currentActId);
  enrolledActs = [];
ngOnInit() {
  this.actService.getAll()
    .subscribe(acts => {
      this.dataSource = new ActivityTableDataSource(this.paginator, this.sort);
      this.dataSource.data = acts;
      this.acts = acts;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
    }
    );
    this.actUpdateForm = this.formBuilder.group({
        currentActName: [''],
        currentActLocation: [],
        currentActDescription: [],
        currentActSDate: [],
        currentActEDate: [],
        currentActDate: [],
        currentActTime: [],
        currentActTeacher: []
    });
}
private loadAllActs() {
  this.actService.getAll().pipe(first()).subscribe(acts => { 
      this.acts = acts; 
      
  });
}
applyFilter(filterValue: string) {
  this.filter$.next(filterValue.trim().toLowerCase());
}
edit(id,name,location,description,sdate,
    edate,date,time,teacher){
        
        this.showEditForm = true;
        this.currentActId = id;this.currentActName = name;this.currentActLocation = location;
        this.currentActSDate = sdate;this.currentActEDate = edate;this.currentActDate = date;
        this.currentActTime = time;this.currentActTeacher = teacher;this.currentActDescription=description
        let checker = true;
        
    }
resetForm(){
    this.actUpdateForm.reset();
}
cancel(){
    this.showEditForm = false;
}

deleteAct(id: number) {
    this.actService.deleteAct(id).pipe(first()).subscribe(() => { 
        window.location.reload();
        });}
deleteAll(){
    this.actService.deleteAllActs();window.location.reload();
}
get f() { return this.actUpdateForm.controls; }
onSubmit(f:NgForm):void {
    alert(f);
    alert(this.currentActName)
}
  


  
}
