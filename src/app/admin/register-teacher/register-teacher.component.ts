import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AlertService, UserService } from 'src/app/_services';
import { TeacherService } from 'src/app/_services/teacher.service';
import { Teacher } from 'src/app/_models/teacher';
import { Router } from '@angular/router';
import { User } from 'src/app/_models';

@Component({
  selector: 'app-register-teacher',
  templateUrl: './register-teacher.component.html',
  styleUrls: ['./register-teacher.component.css']
})
export class RegisterTeacherComponent implements OnInit {
  regTeacherForm: FormGroup;
  submitted = false;
  loading = false;
  teachers: Teacher[] = [];
  users: User[] = [];

  constructor(private formBuilder: FormBuilder,
    private teacherService: TeacherService,
    private alertService: AlertService ,private router: Router,
    private userService: UserService) { }
    

  ngOnInit() {
    this.loadAllTeachers();
    this.loadAllUsers();
    this.regTeacherForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
  });
  }
  private loadAllTeachers() {
    this.teacherService.getAll().pipe(first()).subscribe(teachers => { 
        this.teachers = teachers; 
        
    });
}private loadAllUsers() {
    this.userService.getAll().pipe(first()).subscribe(users => { 
        this.users = users; 
        
    });
}
deleteTeacher(id: number) {
  this.teacherService.deleteTeacher(id).pipe(first()).subscribe(() => {
    // window.location.reload(); 
      this.loadAllTeachers() 
  });
}
deleteUser(id: number) {
    this.userService.delete(id).pipe(first()).subscribe(() => { 
        window.location.reload();
        this.loadAllUsers() 
    });
  }

  onSubmit() {
    this.submitted = true;
    
    // stop here if form is invalid
    if (this.regTeacherForm.invalid) {
        return;
    }
    console.log(this.regTeacherForm);
    this.loading = true;
    this.teacherService.registerTeacher(this.regTeacherForm.value)
        .pipe(first())
        .subscribe(
            data => {
                this.alertService.success('Registration successful', true);
                window.location.reload();

            },or => {
                this.alertService.error("error");
                alert("Username already taken")
                this.loading = false;
            });
}
}
