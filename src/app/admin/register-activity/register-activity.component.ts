import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Activity } from 'src/app/_models/activity';
import { User } from 'src/app/_models';
import { RegisterComponent } from 'src/app/register';
import { UserService, ActivityService, AlertService } from 'src/app/_services';
import { Location } from '@angular/common';
import { TeacherService } from 'src/app/_services/teacher.service';
import { Teacher } from 'src/app/_models/teacher';
@Component({
  selector: 'app-register-activity',
  templateUrl: './register-activity.component.html',
  styleUrls: ['./register-activity.component.css']
})
export class RegisterActivityComponent implements OnInit {
  submitted = false;
  loading = false;
  regActForm: FormGroup;
  regComp : RegisterComponent;
  currentUser: User;
  users: User[] = [];
  acts: Activity[] = [];
  teachers: Teacher[] = [];

  constructor( 
      private formBuilder: FormBuilder,
      private userService: UserService,
      private actservice: ActivityService,
      private alertService: AlertService,
      private teacherService:TeacherService) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

  }

  ngOnInit() {
      this.loadAllUsers();
      this.loadAllActs();
      this.loadAllTeachers();
      this.regActForm = this.formBuilder.group({
          name: ['', Validators.required],
          location: ['', Validators.required],
          description: ['',Validators.required],
          sdate: ['', Validators.required],
          edate: ['', Validators.required],
          date: ['', Validators.required],
          time: ['', Validators.required],
          teacher: ['', Validators.required]
      });
      
  }
  

  deleteUser(id: number) {
      this.userService.delete(id).pipe(first()).subscribe(() => { 
          this.loadAllUsers() 
      });
  }
  deleteAct(id: number) {
      this.actservice.deleteAct(id).pipe(first()).subscribe(() => { 
          this.loadAllActs();
          window.location.reload();
      });
  }

  private loadAllUsers() {
      this.userService.getAll().pipe(first()).subscribe(users => { 
          this.users = users; 
          
      });
  }
  private loadAllActs() {
      this.actservice.getAll().pipe(first()).subscribe(acts => { 
          this.acts = acts; 
          
      });
  }
  private loadAllTeachers() {
    this.teacherService.getAll().pipe(first()).subscribe(teachers => { 
        this.teachers = teachers; 
        
    });
}
  

  onSubmit() {
      this.submitted = true;
      
      // stop here if form is invalid
      if (this.regActForm.invalid) {
          return;
      }
      console.log(this.regActForm);
      this.loading = true;
      this.actservice.registerAct(this.regActForm.value)
          .pipe(first())
          .subscribe(
              data => {
                  window.location.reload();
                  this.alertService.success('Registration successful', true);
                  location.reload();
              },or => {
                  this.alertService.error("error");
                  this.loading = false;
              });
  }
}
