﻿import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_guards';
import { StudentComponent } from './student/student.component';
import { TeacherComponent } from './teacher/teacherHome/teacher.component';
import { ActivityComponent } from './activity/activity.component';
import { HomeAdminComponent } from './admin/home/homeAdmin.component';
import { RegisterActivityComponent } from './admin/register-activity/register-activity.component';
import { ViewActivityComponent } from './admin/adminView-activity/adminView-activity.component';
import { RegisterTeacherComponent } from './admin/register-teacher/register-teacher.component';
import { ActivityDetailComponent } from './admin/activity-detail/activity-detail.component';
import { StudentViewActivityComponent } from './student/student-view-activity/student-view-activity.component';
import { ViewActTeacherComponent } from './teacher/view-act-teacher/view-act-teacher.component';
import { EnrollComponent } from './student/enroll/enroll.component';
import { AboutActivityComponent } from './student/about-activity/about-activity.component';

export const appRoutes: Routes = [
    { path: '', component: HomeAdminComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'student/:id', component: StudentComponent },
    { path: 'teacher/:id', component: TeacherComponent},
    { path: 'admin', component: HomeAdminComponent },
    { path: 'activity', component: ActivityComponent},
    { path: 'registerActivity', component: RegisterActivityComponent},
    { path: 'adminViewActivity', component: ViewActivityComponent},
    { path: 'adminRegisterTeacher', component: RegisterTeacherComponent},
    { path: 'actDetail/:id', component: ActivityDetailComponent }, 
    { path: 'student/:id/studentViewActivity', component: StudentViewActivityComponent},
    { path: 'student/:id/enrolled', component: EnrollComponent},
    { path: 'teacher/:id/teacherViewActivity', component: ViewActTeacherComponent},
    { path: 'student/:id/aboutActivity', component: AboutActivityComponent} 
    // otherwise redirect to home
    // { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);